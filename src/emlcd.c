/***************************************************************************//**
 * @file
 * @brief Energy Mode demo for EFM32ZG_STK3200
 *******************************************************************************
 * # License
 * <b>Copyright 2018 Silicon Laboratories Inc. www.silabs.com</b>
 *******************************************************************************
 *
 * The licensor of this software is Silicon Laboratories Inc. Your use of this
 * software is governed by the terms of Silicon Labs Master Software License
 * Agreement (MSLA) available at
 * www.silabs.com/about-us/legal/master-software-license-agreement. This
 * software is distributed to you in Source Code format and is governed by the
 * sections of the MSLA applicable to Source Code.
 *
 ******************************************************************************/

#include <stdio.h>

#include "em_device.h"
#include "em_chip.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "em_rmu.h"
#include "display.h"
#include "textdisplay.h"
#include "retargettextdisplay.h"

static volatile bool      displayEnabled = false; /* Status of LCD display. */
static DISPLAY_Device_t   displayDevice;    /* Display device handle.         */

/***************************************************************************//**
 * @brief  Main function
 ******************************************************************************/
int main(void)
{
  /* Chip errata */
  CHIP_Init();

  /* Initialize the display module. */
  displayEnabled = true;
  DISPLAY_Init();

  /* Retrieve the properties of the display. */
  if ( DISPLAY_DeviceGet(0, &displayDevice) != DISPLAY_EMSTATUS_OK ) {
    /* Unable to get display handle. */
    while ( 1 ) ;
  }

  /* Retarget stdio to the display. */
  if ( TEXTDISPLAY_EMSTATUS_OK != RETARGET_TextDisplayInit() ) {
    /* Text display initialization failed. */
    while ( 1 ) ;
  }

  printf("\n\n Hola!"
         "\n Causa de Reset");

  // Get reset cause(s)
  uint32_t rflags = RMU_ResetCauseGet();

  if (rflags & RMU_RSTCAUSE_PORST){
  	  printf("\n Power On Rst");
  }
  //Brown Out Detector Unregulated Domain= BOD UD
  if (rflags & RMU_RSTCAUSE_BODUNREGRST){
  	  printf("\n BOD UD Reset");
  }
  //Brown Out Detector Regulated Domain= BOD RD
  if (rflags & RMU_RSTCAUSE_BODREGRST){
  	  printf("\n BOD RD Reset");
  }
  //External pin reset
  if (rflags & RMU_RSTCAUSE_EXTRST){
  	  printf("\n Ext Pin Reset");
  	  GPIO_PinModeSet(gpioPortC, 10, gpioModePushPull, 1);
  }
  if (rflags & RMU_RSTCAUSE_WDOGRST){
  	  printf("\n Watchdog Reset");
  }
  if (rflags & RMU_RSTCAUSE_LOCKUPRST){
  	  printf("\n LOCKUP Reset");
  }
  //System Request Reset
  if (rflags & RMU_RSTCAUSE_SYSREQRST){
  	  printf("\n Syst Req Reset");
  }
  if (rflags & RMU_RSTCAUSE_EM4RST){
  	  printf("\n EM4 Reset");
  }
  if (rflags & RMU_RSTCAUSE_EM4WURST){
  	  printf("\n EM4 Wake-up Rst");
  }
  if (rflags & RMU_RSTCAUSE_BODAVDD0){
  	  printf("\n AVDD0 Bod Rst");
  }
  if (rflags & RMU_RSTCAUSE_BODAVDD1){
  	  printf("\n AVDD1 Bod Rst");
  }

  // Clear reset causes so we know which reset matters next time around
  RMU_ResetCauseClear();

  for (;; ) {
}
}
